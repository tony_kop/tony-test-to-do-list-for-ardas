import React from 'react';
import Task from '../Task';
import arrTasks from '../../../tasks.json';


function TaskList () {
    return (
      <React.Fragment>
        {  arrTasks.map(tsk => <Task task={tsk} />) }
      </React.Fragment>
    )
} 

export default TaskList;
