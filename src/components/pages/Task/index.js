import React from 'react';

function Task({task}) {

  return (
        <tr>
          <td>{task.name}</td>
          <td>Tags</td>
          <td>{task.actual_effort}</td>
          <td>{task.estimated_effor}t</td>
          <td>{(new Date(task.due_date)).toLocaleDateString()}</td>
        </tr>
  )
}

export default Task;