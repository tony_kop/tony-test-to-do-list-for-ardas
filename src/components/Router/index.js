import React from 'react';
import TaskList from '../pages/TaskList';
import TaskPage from '../pages/TaskPage';
import { Switch, Route } from 'react-router-dom';

export default function () {
  return (
    <Switch>
      <Route exact path='/' component={TaskList} />
      <Route path='/task-page' component={TaskPage} />
    </Switch>
  )
}