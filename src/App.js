import React, { Component } from 'react';
import { BrowserRouter } from 'react-router-dom';
import Router from './components/Router';
import './App.scss';

class App extends Component {

  render() {
    console.log('APP');
    return (
      <BrowserRouter>
        <div className="App">
          <table className="tasks">
            <caption>To Do List</caption>
            <tr>
              <th>Name Task</th>
              <th>Tags</th>
              <th>Actual effort</th>
              <th>Estimated effort</th>
              <th>Due date</th>
            </tr>
              <Router />
          </table>
        </div>
      </BrowserRouter>
    );
  }

}


export default App;
