import {combineReducers} from 'redux';

import TaskList from '../components/pages/TaskList/reducer';
import TaskPage from '../components/pages/TaskPage/reducer';

export default combineReducers({
  TaskList,
  TaskPage,
})